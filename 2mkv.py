# -*- coding: utf-8 -*-
"""
package 2mkv.py

 Description : convert file vob and m2ts to mkv

 Set of common module: including message management (to file and stdout)
 Error code began by 200
 Output:
   return Code  : 0 if ok
                  1 otherwise """
from sys import stdout
                  
__version__    = "1.0"
__date__       = "2018/03/20"
__author__     = "Alten SO"

#-------------------- Import part -------------------------------------
# module importation 
#--------------------------------------------------------------------------

import sys,os.path,re,os
import easygui
import glob
import subprocess , shutil, os,win32com
from os.path import basename
from unidecode import unidecode

ffmpegpath=subprocess.run(['where','ffmpeg'], stdout = subprocess.PIPE, stderr=subprocess.PIPE)
if ffmpegpath.returncode == 1:
   easygui.msgbox("ffmpeg must be installed and findable in the path")
   sys.exit(1)
FFMPEG=ffmpegpath.stdout.decode('utf-8').strip()

AudioStreamFormat=["5.1"]
FileFormat=""
prompt=""
targetFile=""
currentfile=""
try:    
    #gather all link files
    linkargs = [ arg for arg in sys.argv[1:]  if '.' not in arg or os.path.splitext(os.path.basename(arg))[1] == '.lnk' ]
    #retrieve targets from links
    linkstringargument =[ win32com.client.Dispatch("WScript.Shell").CreateShortCut(arg).targetpath for arg in linkargs ]
    #gather other selection
    stringargument = [ arg for arg in sys.argv[1:]  if '.' not in arg or os.path.splitext(os.path.basename(arg))[1] != '.lnk' ]+linkstringargument
    
    for currentfile in stringargument:
        FileFormat=""
        srcFolder=os.path.dirname(currentfile)
        currentfileName,file_extension = os.path.splitext(os.path.basename(currentfile))
        FFMPEGCommand='"'+FFMPEG+'"'+'-i '+currentfile
        try:
            #FileFormat=subprocess.check_output([FFMPEG,'-i',currentfile],shell=True)  
            FileFormat=subprocess.run([FFMPEG,'-i',currentfile],  stderr=subprocess.PIPE).stderr.decode('utf-8')
        
        except:
            pass
        Streams=[line.strip() for line in FileFormat.split('\n') if line.strip().startswith('Stream')]
        FullAudioStream= [line for line in Streams if "Audio:" in line ]
        AudioStream=[]
        for audioformat in AudioStreamFormat:
            AudioStream=AudioStream+[re.findall("#(\d:\d)",line)[0] for line in FullAudioStream if audioformat in line ]
        
        VideoStream=[re.findall("#(\d:\d)",line)[0] for line in Streams if "Video:" in line ]
        try:
          SubtitleStream = [re.findall("#(\d:\d)\(.*\): Subtitle:",line)[0] for line in Streams if "Subtitle:" in line ]
        except:
          SubtitleStream=[]
          
        subtitle = False
        subtitleFiles=[]
        stream_cmd=""  #init with video stream
        subtitle_cmd=" "
        subtitle_audio_cmd=""
        Multithread_cmd=' -threads 8 -y '
        for stream in VideoStream+SubtitleStream+AudioStream:
             #add each audio stream to output
            stream_cmd=stream_cmd+" -map "+stream
       
            
        subtitleFolder=srcFolder+"\\"+"Subtitles"
        if os.path.exists(subtitleFolder):
            for Subtitlefile in glob.glob(subtitleFolder+"\\*.*"):
              subtitle_cmd=subtitle_cmd+" -f srt -i "+'"'+Subtitlefile+'"'
               
        targetFile=os.path.abspath(srcFolder+'\\'+currentfileName+".mkv")
        #subprocess.STD
        if file_extension.upper() == '.M2TS':
       
            print("currentfile:%s"%(currentfile))
            #extract chapters from mounted device     
            FFMPEGCommand='"'+FFMPEG+'"'+" -fflags +genpts "+Multithread_cmd+' -i  '+'"'+currentfile+'"'+ subtitle_cmd    +stream_cmd+' -vcodec copy -acodec ac3 -scodec copy  '+'"'+targetFile+'"'
            print(FFMPEGCommand)
            prompt=subprocess.check_output(FFMPEGCommand,shell=True)  
        elif file_extension.upper() == '.VOB':
            
            print("currentfile:%s"%(currentfile))
            #extract chapters from mounted device     
            FFMPEGCommand='"'+FFMPEG+'"'+" -fflags +genpts "+Multithread_cmd+' -i  '+'"'+currentfile+'"'+ subtitle_cmd    +stream_cmd+' -vcodec copy -acodec ac3 -scodec copy  '+'"'+targetFile+'"'
            print(FFMPEGCommand)
            prompt=subprocess.check_output(FFMPEGCommand,shell=True)
            pass
        elif file_extension.upper() == '.MKV':
            """
            ffmpeg -i input.mp4 -f srt -i input.srt \
            -map 0:0 -map 0:1 -map 1:0 -c:v copy -c:a copy \
            -c:s srt  output.mkv"""
            
            targetFile = os.path.abspath(srcFolder+'\\'+currentfileName+"_traget.mkv")
            print("currentfile:%s"%(currentfile))
            #extract chapters from mounted device     
            FFMPEGCommand='"'+FFMPEG+'"'+" -fflags +genpts "+Multithread_cmd+' -i  '+'"'+currentfile+'"'+ subtitle_cmd    +stream_cmd+' -vcodec copy -acodec ac3 -scodec copy  '+'"'+targetFile+'"'
            print(FFMPEGCommand)
            prompt=subprocess.run(FFMPEGCommand,stdout = subprocess.PIPE, stderr=subprocess.PIPE)
            try: 
                prompt.check_returncode()
                
            except:
                easygui.exceptionbox(prompt.stderr.decode("utf8","replace"),"Error while Processing:")
            pass
         

        elif file_extension.upper() == '.MP4':

           
            targetFile=os.path.abspath(srcFolder+'\\'+currentfileName+"_traget.mkv")
            print("currentfile:%s"%(currentfile))
            #extract chapters from mounted device     
            FFMPEGCommand='"'+FFMPEG+'"'+' -threads 10 -y -i  '+'"'+currentfile+'"'+' -map 0:0 -map 0:1 -vcodec copy -acodec ac3 -scodec copy  '+'"'+targetFile+'"'
            print(FFMPEGCommand)
            prompt=subprocess.run(FFMPEGCommand,stdout = subprocess.PIPE, stderr=subprocess.PIPE)
            pass 
        else:
            print("currentfile:%s"%(currentfile))
            #extract chapters from mounted device     
            FFMPEGCommand='"'+FFMPEG+'"'+Multithread_cmd+' -i  '+'"'+currentfile+'"'+ subtitle_cmd    +stream_cmd+' -vcodec copy -acodec ac3 -scodec copy  '+'"'+targetFile+'"'
            print(FFMPEGCommand)
            prompt=subprocess.check_output(FFMPEGCommand,shell=True)    
            pass
    
   # easygui.msgbox(" Terminated ok :o\n Target:%s"%(targetFile))
    choice=easygui.buttonbox(" Terminated ok :o\n Target:%s"%("\n".join(stringargument)),"End of conversion Process", choices=('OK','LOG'),default_choice='OK')
    if choice == 'LOG':
        easygui.textbox(title="Log file of conversion", text=prompt.stderr.decode('utf-8').strip())
except :
     easygui.exceptionbox("Error while processing %s" % (currentfile))   
